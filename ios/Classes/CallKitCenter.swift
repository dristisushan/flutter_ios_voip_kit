//
//  CallKitCenter.swift
//  flutter_ios_voip_kit
//
//  Created by 須藤将史 on 2020/07/02.
//

import Foundation
import CallKit
import UIKit

class CallKitCenter: NSObject {

    private let controller = CXCallController()
    private let iconName: String
    private let localizedName: String
    private let supportVideo: Bool
    private let skipRecallScreen: Bool
    private var provider: CXProvider?
    private var uuid = UUID()
    private(set) var uuidString: String?
    private(set) var title: String?
    private(set) var agoraToken: String?
    private(set) var agoraChannelName: String?
    private(set) var agoraUid: String?
    private(set) var useSandBox: Any?
    private(set) var environment: String?
    private(set) var rcChannelList: String?
    private(set) var rcServerurl: String?
    private(set) var rcRoomId: String?
    private(set) var rcRoomName: String?
    private(set) var rcRoomDescription: String?
    private(set) var userIds: Any?
    private(set) var isPrivate: Any?
    private(set) var callerName: String?
    private(set) var isVideo: Any?
    private(set) var peeqSessionHistoryId: Int?
    private(set) var type: String?
//     private(set) var incomingCallerId: String?
//     private(set) var incomingCallerName: String?
//     private(set) var roomName: String?
//     private(set) var serverUrl: String?
//     private(set) var subject: String?
//     private(set) var title: String?
//     private(set) var type: String?
    private var isReceivedIncomingCall: Bool = false
    private var isCallConnected: Bool = false
    private var maximumCallGroups: Int = 1
    var answerCallAction: CXAnswerCallAction?

    var isCalleeBeforeAcceptIncomingCall: Bool {
        return self.isReceivedIncomingCall && !self.isCallConnected
    }

    override init() {
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist") {
            let plist = NSDictionary(contentsOfFile: path)
            self.iconName = plist?["FIVKIconName"] as? String ?? "AppIcon-VoIPKit"
            self.localizedName = plist?["FIVKLocalizedName"] as? String ?? "App Name"
            self.supportVideo = plist?["FIVKSupportVideo"] as? Bool ?? false
            self.skipRecallScreen = plist?["FIVKSkipRecallScreen"] as? Bool ?? false
            self.maximumCallGroups = plist?["FIVKMaximumCallGroups"] as? Int ?? 1
        } else {
            self.iconName = "AppIcon-VoIPKit"
            self.localizedName = "App Name"
            self.supportVideo = false
            self.skipRecallScreen = false
        }
        super.init()
    }

    func setup(delegate: CXProviderDelegate) {
        let providerConfiguration = CXProviderConfiguration(localizedName: self.localizedName)
        providerConfiguration.supportsVideo = self.supportVideo
        providerConfiguration.maximumCallsPerCallGroup = 1
        providerConfiguration.maximumCallGroups = maximumCallGroups
        providerConfiguration.supportedHandleTypes = [.generic]
        providerConfiguration.iconTemplateImageData = UIImage(named: self.iconName)?.pngData()
        self.provider = CXProvider(configuration: providerConfiguration)
        self.provider?.setDelegate(delegate, queue: nil)
    }

    func startCall(uuidString: String, targetName: String) {
        self.uuid = UUID(uuidString: uuidString)!
        let handle = CXHandle(type: .generic, value: targetName)
        let startCallAction = CXStartCallAction(call: self.uuid, handle: handle)
        startCallAction.isVideo = self.supportVideo
        let transaction = CXTransaction(action: startCallAction)
        self.controller.request(transaction) { error in
            if let error = error {
                print("❌ CXStartCallAction error: \(error.localizedDescription)")
            }
        }
    }

    func incomingCall(uuidString: String, title: String, agoraToken: String,
                      agoraChannelName:String, agoraUid: String, useSandBox: Any,
                       environment: String,rcChannelList: String, rcServerurl: String,
                        rcRoomId: String, rcRoomName: String, rcRoomDescription: String,
                         userIds: Any, isPrivate: Any, callerName: String,
                          isVideo: Any, peeqSessionHistoryId: Int, type: String,
                      completion: @escaping (Error?) -> Void) {
        print("this is uuid"+uuidString);
        self.uuidString = uuidString
//         self.incomingCallerId = callerId
        self.title = title
        self.agoraToken = agoraToken
        self.agoraChannelName = agoraChannelName
        self.agoraUid = agoraUid
        self.useSandBox = useSandBox
        self.environment = environment
        self.rcChannelList = rcChannelList
        self.rcServerurl = rcServerurl
        self.rcRoomId = rcRoomId
        self.rcRoomName = rcRoomName
        self.rcRoomDescription = rcRoomDescription
        self.userIds = userIds
        self.isPrivate = isPrivate
        self.callerName = callerName
        self.isVideo = isVideo
        self.peeqSessionHistoryId = peeqSessionHistoryId
        self.type = type
        self.isReceivedIncomingCall = true

        self.uuid = UUID(uuidString: uuidString)!
        let update = CXCallUpdate()
        update.remoteHandle = CXHandle(type: .generic, value: callerName)
        update.hasVideo = self.supportVideo
        update.supportsHolding = false
        update.supportsGrouping = false
        update.supportsUngrouping = true
        self.provider?.reportNewIncomingCall(with: self.uuid, update: update, completion: { error in
            if (error == nil) {
                self.connectedOutgoingCall()
            }

            completion(error)
        })
    }

    func acceptIncomingCall(alreadyEndCallerReason: CXCallEndedReason?) {
        guard alreadyEndCallerReason == nil else {
            self.skipRecallScreen ? self.answerCallAction?.fulfill() : self.answerCallAction?.fail()
            self.answerCallAction = nil
            return
        }

        self.answerCallAction?.fulfill()
        self.answerCallAction = nil
    }

    func unansweredIncomingCall() {
        self.disconnected(reason: .unanswered)
    }

    func endCall() {
    print("this is self uuid",self.uuid)
        let endCallAction = CXEndCallAction(call: self.uuid)
        let transaction = CXTransaction(action: endCallAction)
        self.controller.request(transaction) { error in
            if let error = error {
                print("❌ CXEndCallAction error: \(error.localizedDescription)")
            }
        }
    }

    func callConnected() {
        self.isCallConnected = true
    }

    func connectingOutgoingCall() {
        self.provider?.reportOutgoingCall(with: self.uuid, startedConnectingAt: nil)
    }

    private func connectedOutgoingCall() {
        self.provider?.reportOutgoingCall(with: self.uuid, connectedAt: nil)
    }

    func disconnected(reason: CXCallEndedReason) {
        self.uuidString = nil
        self.agoraUid = nil
        self.callerName = nil
        self.answerCallAction = nil
        self.isReceivedIncomingCall = false
        self.isCallConnected = false

        self.provider?.reportCall(with: self.uuid, endedAt: nil, reason: reason)
    }
}
